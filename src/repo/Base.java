package repo;

import java.io.File;
import java.util.concurrent.TimeUnit;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.chrome.ChromeOptions;
import org.testng.annotations.AfterTest;
import org.testng.annotations.BeforeTest;

public class Base {

	public static WebDriver dr;
	@BeforeTest
	public void initializedriver()
	{
		String driverpath=System.getProperty("user.dir")+"\\chromedriver.exe";
		System.setProperty("webdriver.chrome.driver",driverpath);
		ChromeOptions options = new ChromeOptions();
		options.addArguments("--disable-notifications");
		dr=new ChromeDriver(options);
			dr.get("https://www.imdb.com/");	
	}
	
	@AfterTest
	public void tearndown()
	{
		dr.manage().timeouts().implicitlyWait(2, TimeUnit.SECONDS);
		dr.quit();
	}
}
