package assignment;

import org.testng.Assert;
import org.testng.annotations.Test;
import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.TimeUnit;

import repo.*;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.PageFactory;

import repo.Uirepo;

public class Taskone extends Base{

	@Test
	public static void main() throws InterruptedException {
		
		try {
		dr.manage().timeouts().implicitlyWait(5, TimeUnit.SECONDS);
		PageFactory.initElements(dr, repo.Uirepo.class);
		Uirepo ui=new Uirepo();
		List<WebElement> actualuilist=ui.imdbsearch(dr);
		ArrayList<String> actualtitles=ui.api(dr);
		Boolean fl=ui.verify(actualtitles, actualuilist);
				Assert.assertTrue(fl);
		}
		catch(Exception e)
		{
			System.out.println(e);
		}

	}

}
